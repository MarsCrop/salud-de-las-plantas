# /usr/bin/env python3
# -*- coding: iso-8859-1 -*-

from cx_Freeze import setup, Executable
setup(
    name = "Salud de cultivos",
    version = "1.0.0",
    options = {"build_exe": {
        'packages': ["pyaudio","tensorflow", 'scikit-learn',"opencv-python", "scikit-image",  "scipy","wxpython", 'numpy','python-dateutil','pytz',"pandas"],
        'include_msvcr': True,
    }},
    executables = [Executable("salud.py",base="Win32GUI")]
    )    
