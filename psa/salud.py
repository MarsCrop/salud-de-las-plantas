import sys
from soil import *
import wx
from threading import Thread
import logging
import warnings
from wx.lib.mixins.listctrl import ListCtrlAutoWidthMixin
from functools import partial
from scipy.misc import imsave
import cv2
import time
warnings.simplefilter("ignore", RuntimeWarning)

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

from sklearn.metrics import confusion_matrix


class AutoWidthListCtrl(wx.ListCtrl, ListCtrlAutoWidthMixin):
    def __init__(self, parent):
        wx.ListCtrl.__init__(self, parent, -1, style=wx.LC_REPORT | wx.FULL_REPAINT_ON_RESIZE | wx.BORDER_SUNKEN)
        ListCtrlAutoWidthMixin.__init__(self)

class streamFrame(wx.Frame): 
    """
    El evento click del recuadro permite capturar una secuencia de imagenes utilizando la captura de video. El dispositivo puede estar conectado remotamente.
    Para eso debe proporcionarse la IP.
    Parametros:                                                                                    
    Se debe seleccionar si la captura sera desde el dispositivo local (una webcam) o una camara IP. Para usar la camara IP debe utilizarse la URL completa indicando el protocolo si es requerido.     
    """ 
    def __init__(self, parent): 
      wx.Frame.__init__(self, parent, wx.NewId(), "Dispositivo de captura")
      self.Bind(wx.EVT_CLOSE, self.on_close)
      self.SetSize((600, 145))      
      panel = wx.Panel(self, wx.ID_ANY, style=wx.FULL_REPAINT_ON_RESIZE)    	
      lblList = ['Dispositivo local (webcam)', 'Dispositivo remoto (Camara IP)'] 
      vbox = wx.BoxSizer(wx.VERTICAL) 		  
      self.rbox = wx.RadioBox(panel, label = 'Selecciona dispositivo de captura', pos = (80,10), choices = lblList, majorDimension = 1, style = wx.RA_SPECIFY_ROWS) 
      self.btn = wx.Button(panel,-1,"Seleccionar dispositivo", pos = (80,40)) 
      vbox.Add(self.rbox,0,wx.ALIGN_CENTER|wx.ALIGN_BOTTOM)  
      icon = wx.EmptyIcon()
      icon.CopyFromBitmap(wx.Bitmap("product.bmp", wx.BITMAP_TYPE_ANY))
      self.SetIcon(icon)
      hbox1 = wx.BoxSizer(wx.HORIZONTAL) 
      l1 = wx.StaticText(panel, -1, "URL de captura (ej. https://localhost:8081/cam.mpeg)") 
		
      hbox1.Add(l1, 1, wx.EXPAND|wx.ALIGN_LEFT|wx.ALL,5) 
      self.t1 = wx.TextCtrl(panel,style = wx.TE_MULTILINE|wx.TE_RICH2) 
		
      hbox1.Add(self.t1,1,wx.EXPAND|wx.ALIGN_LEFT|wx.ALL,5) 
      #self.t1.Bind(wx.EVT_TEXT,self.OnKeyTyped) 
      vbox.Add(hbox1) 
      self.selection = 0
          
      self.btn.Bind(wx.EVT_BUTTON,self.OnClick) 
      vbox.Add(self.btn,0,wx.ALIGN_CENTER|wx.ALIGN_BOTTOM)
      panel.SetSizer(vbox)

    def MakeModal(self, modal=True):          
        if modal and not hasattr(self, '_disabler'):
            self._disabler = wx.WindowDisabler(self)
        if not modal and hasattr(self, '_disabler'):
            del self._disabler 

    def OnClick(self, evt): 
       selection = self.rbox.GetSelection()
       if selection == 0:
           self.selection = 0
       else:                 
           self.selection = self.t1.GetValue()   
       self.MakeModal(False)
       self.Show(False)
       evt.Skip()  

    def on_close(self, evt):
        self.MakeModal(False)
        evt.Skip()  

class MyFrame5(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, "Estimar salud de las plantas")
        self.SetSize((810, 515))
        self.__close_callback = None
        self.Bind(wx.EVT_CLOSE, self.OnClose)

        icon = wx.EmptyIcon()
        icon.CopyFromBitmap(wx.Bitmap("product.bmp", wx.BITMAP_TYPE_ANY))
        self.SetIcon(icon)

        self.Maximize(False)  

        self.__set_properties()

        self.onRun()

    def register_close_callback(self, callback):
        self.__close_callback = callback                                                                              

    def OnClose(self, event):
        doClose = True if not self.__close_callback else self.__close_callback()
        if doClose:
            event.Skip()


    def OnStreamDevice(self, event):        
        self.stream_frame = streamFrame(self)
        self.stream_frame.MakeModal(True)
        self.stream_frame.Show(True)   


    def pic(self): 
        try:                                                                                                            
            selection = self.stream_frame.selection                                                                                       
        except Exception as e:                                                                                                            
            selection = 0
        capture = cv2.VideoCapture(selection)                       
        stopButton = wx.Button(self, label="Capturar", pos=(460,420), size=(100,30))         
        soil_qual_menu = wx.Button(self, label="Dispositivo de captura", pos=(230,420), size=(200,30))                   
        soil_qual_menu.Bind(wx.EVT_BUTTON, self.OnStreamDevice)                                                      
        self.Bind(wx.EVT_BUTTON, self.OnStop, stopButton)                                                                     
        while True:                                                 
            ret, frame = capture.read()                               
            if ret == True:                                         
                 try:                                 
                     imsave('testpicture/test.png',frame) 
                     self.sizer_1 = wx.BoxSizer(wx.HORIZONTAL)
                     self.png = wx.StaticBitmap(self, id=wx.ID_ANY,pos=(0,0),size=(800,400), bitmap=wx.Bitmap('testpicture/test.png',wx.BITMAP_TYPE_ANY))                                                                            
                     self.sizer_1.Add(self.png, 0) 
                     time.sleep(3)                     
                     self.Refresh() 
                 except Exception as e:
                     print(e)


    def onRun(self):   
        td = Thread(target = self.__run)
        td.setDaemon(True)
        td.start() 
                         
    def __run(self):
        self.pic()

    def register_close_callback(self, callback):
        self.__close_callback = callback                                                                              

    def OnClose(self, event):
        doClose = True if not self.__close_callback else self.__close_callback()
        if doClose:
            event.Skip()

    def OnStop(self, callback):
        sickness = predict_sick_plants()
        wx.MessageBox(sickness, "Estado de salud de los cultivos" ,wx.OK | wx.ICON_INFORMATION)  
 
    def OnStreamDevice(self, event):        
        self.stream_frame = streamFrame(self)
        self.stream_frame.MakeModal(True)
        self.stream_frame.Show(True)        

    def __set_properties(self):
        self.SetBackgroundColour(wx.Colour(234, 231, 228))
        self.SetForegroundColour(wx.Colour(0, 0, 0))

# end of class MyFrame

class MyApp(wx.App):
    def OnInit(self):
        self.frame = MyFrame5()
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

# end of class MyApp

def main():
    app = MyApp(0)
    app.MainLoop()

if __name__ == "__main__":
    main()